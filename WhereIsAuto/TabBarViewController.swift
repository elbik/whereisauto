//
//  TabBarViewController.swift
//  WhereIsAuto
//
//  Created by elbik on 23.01.18.
//  Copyright © 2018 elbik. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

	private var autosModel: AutosModel?
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		  autosModel = AutosModel()
		
		  let carOverviewTab = AutosOverviewViewController()
		  carOverviewTab.autosModel = autosModel
		  carOverviewTab.tabBarItem = UITabBarItem(tabBarSystemItem: .history, tag: 0)
		
		  let mapTab = MapViewController()
		  mapTab.autosModel = autosModel
		  mapTab.tabBarItem = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 1)
		
		  self.viewControllers = [carOverviewTab, mapTab]
    }
}
