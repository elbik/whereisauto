//
//  MapViewModel.swift
//  WhereIsAuto
//
//  Created by elbik on 24.01.18.
//  Copyright © 2018 elbik. All rights reserved.
//

import Foundation
import MapKit

class MapViewModel {
	
	private var model: AutosModel
	
	required init(_ autosModel: AutosModel) {
		model = autosModel
	}
	
	internal func getAutosLocations() -> [Placemark] {
		return model.placemarks.map { Placemark(name: $0.name,
															 address: $0.address,
															 fuel: $0.fuel,
															 coordinate: CLLocationCoordinate2D(latitude: $0.coordinates[1], longitude: $0.coordinates[0]),
															 interior: $0.interior.rawValue,
															 exterior: $0.exterior.rawValue)
			
		}
	}
	
}
