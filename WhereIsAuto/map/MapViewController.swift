//
//  MapViewController.swift
//  WhereIsAuto
//
//  Created by elbik on 23.01.18.
//  Copyright © 2018 elbik. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

	private var map: MKMapView?
	private var locationManager: CLLocationManager?
	private var mapViewModel: MapViewModel?
	
	private var memoizedPlaces: [Placemark]?
	
	internal var autosModel: AutosModel? {
		didSet {
			guard autosModel != nil else { return }
			
			mapViewModel = MapViewModel(autosModel!)
		}
	}
	
    override func viewDidLoad() {
		  super.viewDidLoad()

		  map = MKMapView(frame: self.view.frame)
		  map?.delegate = self
		  self.view.addSubview(map!)

		  map?.showsUserLocation = true
		  map?.isZoomEnabled = true
		  map?.isScrollEnabled = true

		  populateAllAutos()
		
		  NotificationCenter.default.addObserver(self, selector: #selector(self.onDataUpdate), name: Notification.Name(Constants.DATA_UPDATED), object: nil)
    }
	
	@objc private func onDataUpdate(notification: Notification) {
		memoizedPlaces = nil
		populateAllAutos()
	}
	
	private func populateAllAutos(){
		if memoizedPlaces == nil {
			memoizedPlaces = mapViewModel?.getAutosLocations() ?? []
		}
		map?.addAnnotations( memoizedPlaces! )
	}
	
	override func viewWillAppear(_ animated: Bool) {
		determineCurrentLocation()
	}

	private func determineCurrentLocation() {
		locationManager = CLLocationManager()
		locationManager?.delegate = self
		locationManager?.desiredAccuracy = kCLLocationAccuracyBest
		locationManager?.requestAlwaysAuthorization()
		
		if CLLocationManager.locationServicesEnabled() {
			 locationManager?.startUpdatingLocation()
		}
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self, name: Notification.Name(Constants.DATA_UPDATED), object: nil)
	}
}

extension MapViewController: MKMapViewDelegate {
	
	func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
		guard let placemark = annotation as? Placemark else { return nil }
		
		let identifier = "marker"
		var view: MKMarkerAnnotationView
		if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
			dequeuedView.annotation = placemark
			view = dequeuedView
		} else {
			view = MKMarkerAnnotationView(annotation: placemark, reuseIdentifier: identifier)
			view.canShowCallout = true
			view.calloutOffset = CGPoint(x: -5, y: 5)
			view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
		}
		
		return view
	}
	
	func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
		if control == view.rightCalloutAccessoryView {
			guard let annotation = view.annotation as? Placemark else { return }
			
			let msg = "address: \(annotation.address)\nfuel: \(annotation.fuel) \n interior: \(annotation.interior) \n exterior: \(annotation.exterior)"
			let alert = UIAlertController(title: annotation.name, message: msg, preferredStyle: .actionSheet)
			alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
			self.present(alert, animated: true, completion: nil)
		}
	}
	
	func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
		guard let annotation = view.annotation as? Placemark else { return }
		
		for annot in (map?.annotations)! {
			if annot.title! != annotation.title! {
				map?.removeAnnotation(annot)
			}
		}
//		map?.removeAnnotations((map?.annotations.filter { $0.title! != annotation.title! } )!)
	}
	
	func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
		map?.removeAnnotations((map?.annotations)!)
		populateAllAutos()
	}
}

extension MapViewController: CLLocationManagerDelegate {
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		let userLocation:CLLocation = locations[0] as CLLocation
		
		let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
		let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
		map?.setRegion(region, animated: true)
		
		locationManager?.stopUpdatingLocation()
	}
}

