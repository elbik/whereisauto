//
//  Placemark.swift
//  WhereIsAuto
//
//  Created by elbik on 24.01.18.
//  Copyright © 2018 elbik. All rights reserved.
//

import Foundation
import MapKit

class Placemark: NSObject, MKAnnotation {
	let name: String
	let address: String
	let fuel: Int
	let coordinate: CLLocationCoordinate2D
	let interior: String
	let exterior: String
	
	init(name: String, address: String, fuel: Int, coordinate: CLLocationCoordinate2D, interior: String, exterior: String) {
		self.name = name
		self.address = address
		self.fuel = fuel
		self.coordinate = coordinate
		self.interior = interior
		self.exterior = exterior
	}
	
	var title: String? {
		return name
	}
	
	var subtitle: String? {
		return address
	}
}


