//
//  AutosOverviewViewController.swift
//  WhereIsAuto
//
//  Created by elbik on 25.01.18.
//  Copyright © 2018 elbik. All rights reserved.
//

import UIKit

class AutosOverviewViewController: UIViewController {

	private var table: UITableView?
	private var overviewViewModel: OverviewListModel?
	
	internal var autosModel: AutosModel? {
		didSet {
			guard autosModel != nil else { return }
			
			overviewViewModel = OverviewListModel(autosModel!)
		}
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		table = UITableView(frame: self.view.frame)
		table?.allowsSelection = false
		self.view.addSubview(table!)
		
		table?.register(CarTableViewCell.nib, forCellReuseIdentifier: CarTableViewCell.identifier)

		if let overviewViewModel = overviewViewModel {
			table?.dataSource = overviewViewModel
		}
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.onDataUpdate), name: Notification.Name(Constants.DATA_UPDATED), object: nil)
    }
	
	@objc private func onDataUpdate(notification: Notification) {
		table?.reloadData()
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self, name: Notification.Name(Constants.DATA_UPDATED), object: nil)
	}
}

