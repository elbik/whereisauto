//
//  CarTableViewCell.swift
//  WhereIsAuto
//
//  Created by elbik on 25.01.18.
//  Copyright © 2018 elbik. All rights reserved.
//

import UIKit

class CarTableViewCell: UITableViewCell {

	static var identifier: String {
		return String(describing: self)
	}
	
	static var nib: UINib {
		return UINib(nibName: identifier, bundle: nil)
	}
	
	@IBOutlet weak var name: UILabel!
	@IBOutlet weak var fuel: UILabel!
	@IBOutlet weak var interior: UILabel!
	@IBOutlet weak var exterior: UILabel!
	
	internal var placemark: PlacemarkVO? {
		didSet {
			
			if let placemark = placemark {
				let getSmileByQuality: (Quality) -> String = { quality in
					return quality == .GOOD ? "👍" : "👎"
				}
				
				name.text = placemark.name
				fuel.text = "⛽️ \(placemark.fuel)"
				interior.text = getSmileByQuality(placemark.interior)
				exterior.text = getSmileByQuality(placemark.exterior)
			}
		}
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
