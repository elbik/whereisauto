//
//  OverviewListModel.swift
//  WhereIsAuto
//
//  Created by elbik on 25.01.18.
//  Copyright © 2018 elbik. All rights reserved.
//

import Foundation
import UIKit

class OverviewListModel: NSObject, UITableViewDataSource {
	
	private var model: AutosModel
	
	required init(_ autosModel: AutosModel) {
		model = autosModel
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return model.placemarks.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if let cell = tableView.dequeueReusableCell(withIdentifier: CarTableViewCell.identifier, for: indexPath) as? CarTableViewCell {
			
			 cell.placemark = model.placemarks[indexPath.row]
			
			 return cell
		}
		return UITableViewCell()
	}
	
}
