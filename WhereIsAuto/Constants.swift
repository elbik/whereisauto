//
//  Constants.swift
//  WhereIsAuto
//
//  Created by elbik on 23.01.18.
//  Copyright © 2018 elbik. All rights reserved.
//

import Foundation

enum Constants {
	static let URL_AUTOS = "http://data.m-tribes.com/locations.json"
	
	static let DATA_UPDATED = "dataUpdated"
}
