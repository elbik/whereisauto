//
//  AutosModel.swift
//  WhereIsAuto
//
//  Created by elbik on 23.01.18.
//  Copyright © 2018 elbik. All rights reserved.
//

import Foundation

class AutosModel {
	
	var placemarks: [PlacemarkVO] = []
	
	required init() {
		
		guard let url = URL(string: Constants.URL_AUTOS) else {
			print("Cannot create URLRequest...")
			return
		}
		var data: Data?
		do {
			let content = try String(contentsOf: url)
			data = content.data(using: .utf8)
		} catch {
			print("Cannot load data from \(Constants.URL_AUTOS)...")
		}
		
		do {
			let decoder = JSONDecoder()
			if let data = data {
				let decodedPlacemarks: PlacemarksVO = try decoder.decode(PlacemarksVO.self, from: data)

				placemarks = decodedPlacemarks.placemarks
				print("DATA is loaded and parsed")
				
				NotificationCenter.default.post(name: Notification.Name(Constants.DATA_UPDATED), object: nil)
			}
			
		} catch {
			print("Cannot decode loaded data.")
		}
	}
}

struct PlacemarksVO : Decodable {
	let placemarks: [PlacemarkVO]
}

struct PlacemarkVO: Decodable {
	let address: String
	let coordinates: [Double]
	let engineType: String
	let exterior: Quality
	let fuel: Int
	let interior: Quality
	let name: String
	let vin: String
	
	enum CodingKeys: String, CodingKey{
		case address
		case engineType
		case fuel
		case name
		case vin
		case exterior
		case interior
		case coordinates
	}
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		address = try values.decode(String.self, forKey: .address)
		engineType = try values.decode(String.self, forKey: .engineType)
		fuel = try values.decode(Int.self, forKey: .fuel)
		name = try values.decode(String.self, forKey: .name)
		vin = try values.decode(String.self, forKey: .vin)
		
		coordinates = try values.decode([Double].self, forKey: .coordinates)
		exterior = try values.decode(Quality.self, forKey: .exterior)
		interior = try values.decode(Quality.self, forKey: .interior)
	}
}

enum Quality: String, Decodable {
	case UNACCEPTABLE
	case GOOD
}

